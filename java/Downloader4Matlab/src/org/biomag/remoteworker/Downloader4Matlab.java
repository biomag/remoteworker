/* DownloaderForMatlab.java
 * 
 * Copyright
 * Krisztián Koós
 * koos.krisztian@brc.mta.hu
 * BIOMAG group
 * Sep 5, 2017
 *
 */
package org.biomag.remoteworker;

import java.io.DataInputStream;
import java.io.IOException;

/*
 * Helper class to receive byte array over a java.io.DataInputStream (eg. from a TCP/IP connection) in MATLAB.
 */
public class Downloader4Matlab {
	public Downloader4Matlab() throws Exception{
		throw new Exception("This class should not be instantiated. Use the static methods.");
	}
	
	public static byte[] receiveData(DataInputStream dis, int len) throws IOException {
		byte[] b = new byte[len];
		dis.readFully(b, 0, len);
		return b;
	}
}
