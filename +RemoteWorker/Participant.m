classdef Participant < handle
    %PARTICIPANT Superclass of the matlab implementation of remote worker
    %   
    
    properties
    end
    
    methods
        function this = Participant()
            persistent downloaderLoaded
            if isempty(downloaderLoaded) || downloaderLoaded == false
                [folder, ~, ~] = fileparts(mfilename('fullpath'));
                javaaddpath(fullfile(folder, '..', 'Downloader4Matlab.jar'));
                downloaderLoaded = true;
            end
        end
    end
    
    methods (Static, Access = protected)
        function jsonobj = receiveJSON(dataInputStream)
            import java.io.*;
            assert(isa(dataInputStream, 'java.io.DataInputStream'));

            byteStream = ByteArrayOutputStream();

            c = dataInputStream.readByte();
            while c ~= int8(newline)
                byteStream.write(c);
                c = dataInputStream.readByte();
            end
            jsonobj = jsondecode(char(byteStream.toString()));
        end
        
        function sendJSON(dataOutputStream, s)
            import java.io.*;
            assert(isa(dataOutputStream, 'java.io.DataOutputStream'));
            jsonobj = [jsonencode(s), newline];
            jsonobj = int8(jsonobj);
            dataOutputStream.write(jsonobj(:), 0, numel(jsonobj));
            dataOutputStream.flush();
            
        end
        
        function data = receiveData(dataInputStream, shape, type, numDataBytes)
            assert(isa(dataInputStream,'java.io.DataInputStream'));
            
            data = org.biomag.remoteworker.Downloader4Matlab.receiveData(dataInputStream,int32(numDataBytes));
            if ~strcmp(type, 'int8')
                data = typecast(data, type);
            end
            data = reshape(data, shape);
        end
        
        function sendData(dataOutputStream, data, info)
            assert(isa(dataOutputStream, 'java.io.DataOutputStream'));
            if nargin < 3
                info = whos('data');
            end
            if ~strcmp(info.class, 'int8')
                data = typecast(data(:), 'int8');
            end
            dataOutputStream.write(data(:), 0, info.bytes);
            
            dataOutputStream.flush();
        end
    end
    
end

