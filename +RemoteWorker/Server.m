classdef Server < RemoteWorker.Participant
    %SERVER Server side of remote worker
    %   
    
    properties (Constant)
        defaultPort = 7878;
    end
    
    properties
        port
        working
    end
    
    properties (Access = protected)
        jobId
        jobStatus
        resultData
        registeredCommands
        commandFunctions
    end
    
    methods
        function this = Server()
            this.port = this.defaultPort;
            this.jobId = uint64(0);
            this.working = false;
            this.registeredCommands = {};
            this.commandFunctions = {};
        end
        
        function run(this)
            import java.net.*;
            import java.io.*;

            this.working = true;
            serverSocket = ServerSocket(this.port);
            while this.working
                clientSocket = serverSocket.accept();
                dout = DataOutputStream(clientSocket.getOutputStream());
                dis = DataInputStream(clientSocket.getInputStream());
                
                commandHeader = this.receiveJSON(dis);
                
                commandData = [];
                if commandHeader.numDataBytes > 0
                    commandData = this.receiveData(dis, commandHeader.dataShape', commandHeader.dataType, commandHeader.numDataBytes);
                end
                disp(['Command received: ', commandHeader.command]);
                if ~this.isGeneralCommand(commandHeader)
                    this.jobId = this.jobId + 1;
                    this.jobStatus = 'running';
                    this.resultData = [];
                end
                [answerHeader, answerData] = this.checkAndCreateAnswer(commandHeader, commandData);
                this.sendJSON(dout,answerHeader);
                if isfield(answerHeader, 'numDataBytes') && answerHeader.numDataBytes > 0
                    this.sendData(dout, answerData);
                end
                clientSocket.close();
                
                if ~this.isGeneralCommand(commandHeader) && strcmp('running', this.jobStatus) % if command looks good, run the associated function
                    try
                        commandFcn = this.getCommandFunction(commandHeader.command);
                        this.resultData = commandFcn(commandHeader, commandData);
                        this.jobStatus = 'done';
                    catch ex
                        this.jobStatus = 'error';
                        disp(['Could not finish job: ', ex.message]);
                    end
                end
                
%                 this.working = false;
            end
            serverSocket.close();
        end
        
        function registerCommand(this, command, fcn)
            assert(ischar(command));
            assert(isa(fcn,'function_handle'));
            if this.isCommandRegistered(command)
                error('Command is already registered! Unregister it first.');
            end
            this.registeredCommands{end+1} = command;
            this.commandFunctions{end+1} = fcn;
        end
        
        function unregisterCommand(this, command)
            assert(ischar(command));
            idx = find(strcmp(command, this.registeredCommands),1);
            if ~isempty(idx)
                this.registeredCommands = {this.registeredCommands{1:idx-1}, this.registeredCommands{idx+1:end}};
                this.commandFunctions = {this.commandFunctions{1:idx-1}, this.commandFunctions{idx+1:end}};
            end
        end
        
        function tf = isCommandRegistered(this, command)
            assert(ischar(command));
            tf = false;
            if ~isempty(this.registeredCommands) && any(strcmp(command, this.registeredCommands))
                tf = true;
            end
        end
        
        function stop(this)
            this.working = false;
        end
        
        function set.jobStatus(this, value)
            assert(ischar(value));
            assert(any(strcmp(value, {'done', 'error', 'running'})));
            this.jobStatus = value;
        end
        
        function set.working(this, value)
            assert(islogical(value));
            this.working = value;
        end
    end
    
    methods (Access = private)
        function [answerHeader, answerData] = checkAndCreateAnswer(this, commandHeader, commandData)
            switch commandHeader.command
                case 'status'
                    [answerHeader, answerData] = this.createAnswerForStatus(commandData);
                case 'getresult'
                    if strcmp(this.jobStatus, 'done')
                        result = this.resultData; %#ok<NASGU>
                        info = whos('result');
                        answerHeader = struct('status', this.jobStatus, 'dataShape', info.size, 'dataType', info.class, ...
                            'numDataBytes', info.bytes);
                        answerData = this.resultData;
                    else
                        [answerHeader, answerData] = this.createAnswerForStatus(commandData);
                    end
                otherwise
                    [answerHeader, answerData] = this.processSpecialRequest(commandHeader);
            end
        end
        
        function [answerHeader, answerData] = createAnswerForStatus(this, commandData)
            answerData = [];
            if commandData == this.jobId
                answerHeader = struct('status', this.jobStatus);
            else
                answerHeader = struct('status', 'wrongJobId');
            end
        end
        
        function [answerHeader, answerData] = processSpecialRequest(this, commandHeader)
            answerData = [];
            if ~this.isCommandRegistered(commandHeader.command)
                this.jobStatus = 'error';
                answerHeader = struct('jobId', this.jobId, 'status', this.jobStatus, 'message', 'Unregistered command');
            else
                answerHeader = struct('jobId', this.jobId, 'status', this.jobStatus);
            end
        end
        
        function commandFcn = getCommandFunction(this, command)
            idx = find(strcmp(command, this.registeredCommands),1);
            commandFcn = this.commandFunctions{idx};
        end
    end
    
    methods (Static)
        function tf = isGeneralCommand(commandHeader)
            tf = false;
            if any(strcmp(commandHeader.command, {'status', 'getresult'}))
                tf = true;
            end
        end
    end
    
end

