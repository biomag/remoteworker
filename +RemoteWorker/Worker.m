classdef Worker < RemoteWorker.Participant
    %WORKER Client side of remote worker
    %   
    
    properties
        host
        port
    end
    
    properties (SetAccess = protected)
        jobId
        answer
        resultData
    end
    
    methods
        function this = Worker()
            this.port = RemoteWorker.Server.defaultPort;
        end
        
        function sendJob(this, command, data)
            this.sendCommand(command, data);
            this.jobId = this.answer.jobId;
        end
        
        function tf = isJobReady(this)
            this.askStatus();
            tf = false;
            if strcmp('done', this.answer.status)
                tf = true;
            end
        end
        
        function tf = isJobFailed(this)
            this.askStatus();
            tf = false;
            if strcmp('error', this.answer.status)
                tf = true;
            end
        end
        
        function result = downloadResult(this)
            this.sendCommand('getresult', this.jobId);
            result = this.resultData;
        end
        
        function set.host(this, value)
            assert(ischar(value));
            this.host = value;
        end
        
        function set.port(this, value)
            assert(isnumeric(value));
            this.port = value;
        end
    end
    
    methods (Access = protected)
        function sendCommand(this, command, data)
            import java.io.*;
            import java.net.*;
            
            assert(ischar(command));
            
            info = whos('data');
            header = struct('command', command, 'dataShape', info.size, 'dataType', info.class, ...
                'numDataBytes', info.bytes);
            sock = Socket(this.host, this.port);
            dis = DataInputStream(sock.getInputStream());
            dout = DataOutputStream(sock.getOutputStream);
            
            this.sendJSON(dout,header);
            if info.bytes > 0
                this.sendData(dout, data, info);
            end
            
            this.answer = this.receiveJSON(dis);
            this.resultData = [];
            if isfield(this.answer, 'numDataBytes') && this.answer.numDataBytes > 0
                this.resultData = this.receiveData(dis, this.answer.dataShape', this.answer.dataType, this.answer.numDataBytes);
            end
            
            sock.close();
        end
        
        function askStatus(this)
            this.sendCommand('status', this.jobId);
        end
    end
    
end

